# important Update:

Due to weird errors when changing frames within the browser, we will have to run "selenium-standalone start" in a seperate terminal and the "npm run test" in another... because this resolves stuff????


# Show Case Readme

Feel free to copy this entire project as a starting point for cucumber tests. Please place the project in a project folder, to make it easier to follow this guide. 

## Installing
Download node.js from https://nodejs.org/en/download/
Install node.js

Install the project specific node modules by opening a command prompt in the project folder
and run npm install.
The project uses some npm modules that sometimes cause issues during installation,
namely `node-gyp`.
I think running 
`npm install --global --production windows-build-tools`
Might solve the issue.


## Building & Running
Simply run `npm test`
The command is specified in `package.json`, and additional commands should be added here if needed.
IMPORTANT: Don't move the window that opens automatically, as the test steps may now work then and you will get an error message like "unknown error: Element is not clickable at point (816, 659)".

## Project Structure

All source code files are kept in the `src` folder.
In here, step definitions should be kept in the `steps` folder,
and helper functions should be kept in `support`.

src
-> steps
----> given 
----> then
----> when
-> support



##How to use the UImap:

Import class in all step-definition files with: import UImap from "../UImap/UImap" 

Everytime you encounter a new element, which will/might be recurring in the future, you can add it to the UImap class.

EXAMPLE 1:
You found that a button, which could be called the continue-button, occurs on almost every UI tested.
Therefore, you add it to the UImap.ts with:

    	path["continue-button"] = "va-scenario-bottombar>#nextSlideButton>#button"

In the feature files, you are now able to refer to this short label, e.g.

	Then I click the 'continue-button'

Instead of 

	Then I click the 'va-scenario-bottombar>#nextSlideButton>#button'


**NOTE:** there will be some recurring UI elements which have inconsistent paths, i.e. can't be determined by some other info (like the URL). Solution?



