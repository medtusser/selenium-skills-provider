Feature: Full test of Flow in VA responder
# For documentation on the flow, see: 

	Scenario: Lauch through Configurator
		Given I start the webpage 'configurator.html'
		Then I click the 'conf-concept-c'
		Then I scroll to (0,1000)
		Then I wait 2 seconds
		Then I click the 'conf-virtual-manikin'
		Then I click the 'conf-start-course'

#CORE SKILLS INTRODUCTION
	Scenario: Welcome page
		When I see if the text in 'va-title' is similar to 'Compressions Warmup'
		#not yet implemented

	Scenario: Hardware Checklist
		#not yet implemented

#ADULT COMPRESSIONS
	Scenario: Overview
		#not yet implemented

## WARM UP LANE
	Scenario: SCO Introduction
	
	Scenario: Warm Up - 20 Compressions 
		# score < 65

	Scenario: Simple Debriefing

## HELP LANE
	Scenario: Introduction
	
	Scenario Outline: Practice - Fail 3x - Coffee Break
		# fail
		# click continue
		# sent back to intro
		# click continue
		# fail
		# repeat 3x

	Scenario: Practice - Pass
		#click previous to go back to Introduction

## ASSESSMENT LANE
	Scenario: Assessment Introduction

	Scenario Outline: Assessment - Fail 3x - Coffee Break

	Scenario: Assessment - Pass

## CERTIFICATE
	Scenario: Certificate Content
		#not yet implemented
