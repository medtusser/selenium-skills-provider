Feature: Unsuccesful Connection

 Scenario: As a user, sometimes the connection to the manikin won't work
	#Given i Launch the course
	#Then i see the video and click continue
	#Then i see it is searching for manikin
	#Then i see no manikin is found
	#Then i click attempts
	#Then i click back to connectiong screen
	#Then i see no manikin is found

    Given I login on CDP: user '0'
	Then I find my course 'RQI AHA 4 Core WebSkills (dev)'
	Then the 'manikin-icon' is visible	
	Then I wait 5 seconds
	Then I click the 'continue-button'
	Then I see if the text in 'Search-for-manikin-box' is similar to 'Searching for manikin...'
	Then I click the 'attempts-button1'
	Then I see if the text in 'Attempts-topbar' is similar to 'Date'
	#I can't hit this button. 
	Then I click the 'Attempts-back-button'
	Then I see if the text in 'Search-for-manikin-box' is similar to 'Searching for manikin...'