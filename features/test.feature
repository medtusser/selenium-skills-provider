Feature: Full Flow Test - Passing Q4 with perfect score
# For documentation on the flow, see: 

	Scenario: Lauch CMS
		Given I start the CMS
		Then I login to the CMS
		Then I try that chai thingy
		Then I enter the course 'without' progress
		Then I check that the continue-button is 'disabled'

#	Scenario Outline: Baseline Check course statuses
#		Then I check that the <module> is <status>
#
#		Examples:
#		    |module				    |status 	| 
#			|'BL-introduction'	    |'enabled'  |
#			|'BL-adult-child-comp'  |'disabled' |
#			|'BL-adult-child-vent'  |'disabled' |
#			|'BL-infant-comp'		|'disabled' |
#			|'BL-infant-vent'		|'disabled' |
#			|'BL-debriefing'		|'disabled' |


	Scenario: Check video controls
		Then I check the video controls
		Then I wait for the video to finish
		Then I check that the continue-button is 'enabled'
		Then I click the 'continue-button'


#	Scenario Outline: Check the Baseline Overview page
#		Then I check that the baseline overview module <module> is <status>
#
#		Examples:
#		  |module						  |status 	  | 
#		  |'overview-adult-child-comp'    |'active'   |
#		  |'overview-adult-child-vent'    |'inactive' |
#		  |'overview-infant-comp'		  |'inactive' |
#		  |'overview-infant-vent'		  |'inactive' |


	Scenario: Baseline Activity - Adult and Child Compressions
		Then I wait 3 seconds
		Then I click the 'continue-button'
		#Then I see if the text in 'BL-overview-top-heading' is similar to 'Baseline / '
		#Then I see if the text in 'BL-overview-sub-heading' is similar to 'Overview: Baseline'
		#Then I see if the text in 'BL-overview-top-heading' is similar to 'Baseline / Adult and Child Compressions'
		#Then I see if the text in 'BL-overview-sub-heading' is similar to 'Baseline: Perform 60 continuous compressions'		
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		Then I set the virtual manikins 'VM-comp-depth' settings to '55'
		Then I set the virtual manikins 'VM-comp-release' settings to '0'
		Then I set the virtual manikins 'VM-comp-rate' settings to '110'		
		Then I 'start' the virtual manikin
		Then I wait for the 'baseline' activity to finish


	#Scenario Outline: Check the Baseline Overview page
		#Then I check that the baseline overview module <module> is <status>
#
		#Examples:
		  #|module						  |status 	  | 
		  #|'overview-adult-child-comp'    |'completed'|
		  #|'overview-adult-child-vent'    |'active'   |
		  #|'overview-infant-comp'		  |'inactive' |
		  #|'overview-infant-vent'		  |'inactive' |


	Scenario: Baseline Activity - Adult and Child Ventilations
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Baseline Activity'
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		Then I set the virtual manikins 'VM-vent' settings to 'enabled'
		Then I set the virtual manikins 'VM-vent-volume' settings to '450'
		Then I set the virtual manikins 'VM-vent-rate' settings to '10'		
		Then I 'start' the virtual manikin
		Then I wait for the 'baseline' activity to finish


	#Scenario Outline: Check the Baseline Overview page
		#Then I check that the baseline overview module <module> is <status>
#
		#Examples:
		  #|module						  |status 	  | 
		  #|'overview-adult-child-comp'    |'completed'|
		  #|'overview-adult-child-vent'    |'completed'|
		  #|'overview-infant-comp'		  |'active'   |
		  #|'overview-infant-vent'		  |'inactive' |


	Scenario: Baseline Activity - Infant Compressions
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Baseline Activity'
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		Then I set the virtual manikins 'VM-type-infant' settings to 'enabled'		
		Then I set the virtual manikins 'VM-comp' settings to 'enabled'
		Then I set the virtual manikins 'VM-comp-depth' settings to '40'
		Then I set the virtual manikins 'VM-comp-release' settings to '0'
		Then I set the virtual manikins 'VM-comp-rate' settings to '110'		
		Then I 'start' the virtual manikin
		Then I wait for the 'baseline' activity to finish


	#Scenario Outline: Check the Baseline Overview page
		#Then I check that the baseline overview module <module> is <status>
#
		#Examples:
		  #|module						  |status 	  | 
		  #|'overview-adult-child-comp'    |'completed'|
		  #|'overview-adult-child-vent'    |'completed'|
		  #|'overview-infant-comp'		  |'completed'|
		  #|'overview-infant-vent'		  |'active'   |


	Scenario: Baseline Activity - Infant Ventilations
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Baseline Activity'
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		Then I set the virtual manikins 'VM-type-infant' settings to 'enabled'
		Then I set the virtual manikins 'VM-vent' settings to 'enabled'
		Then I set the virtual manikins 'VM-vent-volume' settings to '30'
		Then I set the virtual manikins 'VM-vent-rate' settings to '15'		
		Then I 'start' the virtual manikin
		Then I wait for the 'baseline' activity to finish


	#Scenario Outline: Check the Baseline Overview page
		#Then I check that the baseline overview module <module> is <status>
#
		#Examples:
		  #|module						  |status 	  | 
		  #|'overview-adult-child-comp'    |'completed'|
		  #|'overview-adult-child-vent'    |'completed'|
		  #|'overview-infant-comp'		  |'completed'|
		  #|'overview-infant-vent'		  |'completed'|



# RASS-5759: More-Details button js broken		 

	#Scenario Outline: Check Baseline Results
		#Then I click the <debriefing>
		#Then I check that the 'BL-simple-debrief-result' score is 'equal to' <#score>
		#Then I click the 'BL-advanced-debrief'
		#Then I check that the 'BL-advanced-debrief-result' score is 'equal to' <#score>
		#Then I click the 'BL-advanced-debrief-close-button'
#
		#Examples:
		  #|debriefing				  |score| 
		  #|'BL-adult-comp-debrief'    |'100'|
		  #|'BL-adult-vent-debrief'    |'100'|
		  #|'BL-infant-comp-debrief'	  |'100'|
		  #|'BL-infant-vent-debrief'	  |'100'|


	Scenario: Proceed to Core Skills
		Then I click the 'continue-button'
		

	Scenario Outline: Core Skills Check course statuses
		Then I check that the <module> is <status>

		Examples:
		    |module				      |status 	 | 
			|'CORE-adult-child-comp'  |'enabled' |
			|'CORE-adult-child-vent'  |'enabled' |
			|'CORE-infant-comp'		  |'enabled' |
			|'CORE-infant-vent'		  |'enabled' |
			|'CORE-adult-child-comp'  |'enabled' |



	Scenario: ASSESSMENT - Passing Adult and Child COMPRESSIONS
		Then I wait 3 seconds	
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Assessment'
		#Then I see if the text in 'instruction-page-activity-title' is similar to 'Adult and Child Compressions'
		Then I set the virtual manikins 'VM-comp-depth' settings to '55'
		Then I set the virtual manikins 'VM-comp-release' settings to '0'
		Then I set the virtual manikins 'VM-comp-rate' settings to '110'		
		Then I 'start' the virtual manikin
		Then I wait for the 'assessment' activity to finish

		Then I check that the 'simple-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief'
		Then I check that the 'advanced-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief-close-button'
		Then I click the 'continue-button'



	Scenario: ASSESSMENT - Passing Adult and Child VENTILATIONS
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Assessment'
		#Then I see if the text in 'instruction-page-activity-title' is similar to 'Adult and Child Ventilation'
		Then I set the virtual manikins 'VM-vent' settings to 'enabled'
		Then I set the virtual manikins 'VM-vent-volume' settings to '450'
		Then I set the virtual manikins 'VM-vent-rate' settings to '10'			
		Then I 'start' the virtual manikin
		Then I wait for the 'assessment' activity to finish

		Then I check that the 'simple-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief'
		Then I check that the 'advanced-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief-close-button'
		Then I click the 'continue-button'




	Scenario: ASSESSMENT - Passing Infant COMPRESSIONS
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Assessment'
		#Then I see if the text in 'instruction-page-activity-title' is similar to 'Infant Compressions'
		Then I set the virtual manikins 'VM-type-infant' settings to 'enabled'		
		Then I set the virtual manikins 'VM-comp' settings to 'enabled'
		Then I set the virtual manikins 'VM-comp-depth' settings to '40'
		Then I set the virtual manikins 'VM-comp-release' settings to '0'
		Then I set the virtual manikins 'VM-comp-rate' settings to '110'	
		Then I 'start' the virtual manikin
		Then I wait for the 'assessment' activity to finish

		Then I check that the 'simple-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief'
		Then I check that the 'advanced-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief-close-button'
		Then I click the 'continue-button'



	Scenario: ASSESSMENT - Passing Infant VENTILATIONS
		Then I activate the virtual manikin
		Then I click the 'continue-button'
		#Then I see if the text in 'instruction-page-title' is similar to 'Assessment'
		#Then I see if the text in 'instruction-page-activity-title' is similar to 'Infant Ventilation'
		Then I set the virtual manikins 'VM-type-infant' settings to 'enabled'
		Then I set the virtual manikins 'VM-vent' settings to 'enabled'
		Then I set the virtual manikins 'VM-vent-volume' settings to '30'
		Then I set the virtual manikins 'VM-vent-rate' settings to '15'		
		Then I 'start' the virtual manikin
		Then I wait for the 'assessment' activity to finish

		Then I check that the 'simple-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief'
		Then I check that the 'advanced-debrief-result' score is 'equal to' '100'
		Then I click the 'advanced-debrief-close-button'
		Then I click the 'continue-button'


	Scenario: Course completed, now save and exit
		Then I click the 'continue-button'
		Then I see that the course has finished