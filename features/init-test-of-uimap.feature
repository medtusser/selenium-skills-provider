Feature: Simple Video test

 Scenario: As a tester I can make a showcase test
  Given I start the webpage '?config=video-test-configuration#slide01'
  Then I wait 10 seconds
  Then I click the 'mute-button'
  Then I see if the text in 'title' is similar to 'Welcome'
  When I search for 'cute cats'
  Then I can see some search results



