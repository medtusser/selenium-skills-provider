Feature: Log in to CDP	

 Scenario: As a tester I can make a showcase test
  Given I login on CDP: user '0'
  Then I find my course 'RQI AHA 4 Core WebSkills (dev)'
  Then I wait 5 seconds
  #Then I click the 'pause-button'
  #Then I click the 'play-button'
  #Then I click the 'mute-button'
  #Then I click the 'replay-button'
  Then I click the 'progress-bar'
  Then I wait for the video to finish: 'video-player'
  Then I activate the virtual manikin
  Then I wait 5 seconds
 #When I scroll to (0,1000)
  Then I click the 'continue-button'
  Then I wait 2 seconds
  Then I wait 200 seconds
  Then I wait 5 seconds
  Then I click the 'back-button'
  Then I activate virtual manikin
