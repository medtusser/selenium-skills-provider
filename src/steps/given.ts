const chai = require("chai")
var { Given, Then, When } = require('cucumber');
import UImap from "../Support/UImap"

/// <reference path="../../custom.d.ts" />

    /**
     * collapse the 3 steps into 1, such as
     * Given I have starte the '(course-name)' course '(with|without), progress
     */


    Given(
        /^I start the CMS/, 
        function () {
            //const webPage ="http://moodle331.northeurope.cloudapp.azure.com/";
            const webPage = "https://service.contentservice.net/#/contentmap";
            //const webPage = "https://cs-prodtest.contentservice.net/launch_aicc?aicc_sid=3e9b6a91172e5e25553d78596d05d3dc9bc237970df52584250b8dbf805b84dd6e5d3532ab56142bf38faf135e38f107e0678f2ba523f109123ada009a2da1104d80af7179945255dc5e90d7d0e17eb4&aicc_url=https://service.contentservice.net/lms";
            //const webPage = "https://cs3n-staging.contentservice.net/launch_aicc?aicc_sid=3e9b6a91172e5e25553d78596d05d3dc9bc237970df52584250b8dbf805b84dd6e5d3532ab56142bf38faf135e38f107e0678f2ba523f109123ada009a2da1104d80af7179945255dc5e90d7d0e17eb4&aicc_url=https://service.contentservice.net/lms";
            browser.url(webPage);
            browser.wait(3)
            
        }
    );


    Given(
        /^I login to the CMS/, 
        function () {

            // LOGIN FOR MOODLE
            /*
            browser.click('//*[@id="page-wrapper"]/header/div/div[2]/span/a');
            var result = browser.execute(function(){
                document.querySelector('#username').setAttribute('value','dkjens');
                document.querySelector('#password').setAttribute('value','Laerdal1!');
                return "Has to return something, otherwise it breaks...";
            });
            browser.click('//*[@id="loginbtn"]');
            if (browser.getUrl() == 'http://moodle331.northeurope.cloudapp.azure.com/my/') {
                
            }
            else {
                throw new Error(`Login failed: Invalid credentials`)
            }
            */


            //LOGIN FOR https://service.contentservice.net/#/contentmap
            browser.setValue('#i0116','glen.videmark@laerdal.com')
            browser.click('#idSIButton9')
            browser.wait(5)
            
        }
    );


    Given(
        /^I enter the course '(with|without)' progress/,
        function(progress) {
            /*
            Should take couple of params, like
            - courseName
            - server (dev/test/..)
            - with progress / without progress
            - full-screen / small-screen
            Will do later
            */


            /*
            START course via contentservice..
            */
            //click course-menu
            browser.wait(2)
            browser.click('body > nav > div > ul > li:nth-child(5) > a')
            //click sepcific course (skills_2015_rqi_web_q4)
            //browser.click('body > div > table > tbody > tr:nth-child(403) > td:nth-child(1) > a:nth-child(6)');
            browser.wait(2)
            //click specific course (skills_2015_rqi_web_prep_q4)
            //browser.click('body > div > table > tbody > tr:nth-child(401) > td:nth-child(1) > a:nth-child(6)')
            browser.click('body > div > table > tbody > tr:nth-child(402) > td:nth-child(1) > a:nth-child(6)');
            //open drop-down
            //browser.click('#dropdownMenu2')
            //click dev server
            //browser.click('#launchDialog > div > div > div.modal-body > form > div:nth-child(2) > div > ul > li:nth-child(3) > a')
            //click LTI launch
            browser.wait(2)
            browser.click('#launchDialog > div > div > div.modal-body > form > div:nth-child(4) > div > button:nth-child(2)')
            //click user-defined
            browser.wait(2)
            browser.click('#wrapper > div:nth-child(1) > div > button.btn.btn-sm.btn-default')
            browser.wait(3)

            
            //set randrom name
            var randomText = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 10; i++){
              randomText += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            console.log(randomText)
    
            browser.setValue('#wrapper > div:nth-child(5) > input',randomText)            
            browser.click('#launchDialog > div > div > div.modal-footer > button.btn.btn-danger')
            browser.wait(5)
            browser.switchTab(browser.getTabIds()[1])




            /*
            Cannot reset course progress... We need a solution for this if we're to test via Moodle.
            */
            /*
            START course via moodle..
            */
            /*
            console.log(1)
            browser.click('#nav-drawer > nav:nth-child(1) > a:nth-child(2)');
            console.log(2)
            browser.click('#frontpage-course-list > div > div:nth-child(20) > div.info > h3 > a');
            console.log(3)
            
            if(progress=='without'){
                browser.click('#module-262 > div > div > div:nth-child(2) > span > form > div > button')
                browser.wait(20)
            }
            
            // CDPQ4 - development
            browser.click('#module-262 > div > div > div:nth-child(2) > div > a');
            browser.click('#scormviewform > input.btn.btn-primary')
            browser.switchTab(browser.getTabIds()[1])
            browser.click('#collapse1 > div > div > div:nth-child(4) > a')          
            browser.windowHandleMaximize();            
            //browser.wait(50)
            */    

            

        }

    );



