const chai = require("chai");
const expect = require("chai").expect
var { Given, Then, When } = require('cucumber');

import UImap from "../Support/UImap"
import frame from "../Support/frameCheck"
import pageReload from "../Support/pageReload"

/// <reference path="../../custom.d.ts" />

/**
 * This file is for moving/editing the step-definitions one step at a time
 * 
 */

 /**
  * Given
  */
    Given(
        /^I complete the Baseline activities/,
        function () {}
    );

    Given(
        /^I complete the Core Skills activities/,   
        function () {}
    );
    

 /**
  * When
  */
    When(
        /^fdsjmhmmhjhgj/,
        function () {}
    );


 /**
  * Then
  */
    Then(
        /^fdfsdfsdf/,
        function () {}
    );

    Then(
        /^the '([^']+)' is '(enabled|disabled|completed)'$/,
        // Check the side-menu SCO's
        function () {}
    );    