const chai = require("chai");
const expect = require("chai").expect

var { Given, Then, When } = require('cucumber');

import UImap from "../Support/UImap"
import frame from "../Support/frameCheck"
import pageReload from "../Support/pageReload"

/// <reference path="../../custom.d.ts" />



    Then(
        /^I find my course '([^']+)'$/, 
        function (course) {
        	let startDiv = 2;
        	for (var i = 0; i < 10; i++	) {
        		let div = "#accordion > div:nth-child(" + startDiv + ") > div > div > div > div:nth-child(1)";
        		if (browser.getText(div)==course) {
                    break;
                }
        		startDiv++;
        	}
        	browser.click("#accordion > div:nth-child(" + startDiv +") > div > div > div > div.cellDiv.lastCell > a > span"); 
        	browser.click("#collapse1 > div > div > div.cellDiv.lastCell > a");
        	browser.frame("basicltiLaunchFrame");
        	browser.frame("frame");	
            browser.wait(3);
        	
        }
    );



    Then(
        /^I debug$/, 
        function (course) {
           browser.debug();
        }
    );

    //This function is for check if the URL is right. To check if the simulator is at the right place
    Then(
        /^the URL ends with '([^']+)'$/, 
        function (urlEnd) {
            const URL = browser.getUrl();
            const test = URL.match(new RegExp("^.+" + urlEnd + "$"));
            if (test != null) {
                  
            }
            else {
                throw new Error(`fail`)
            }
        }
    );


    //This is an implicit wait function in seconds
    Then(
        /^I wait (\d+) seconds$/,
        function (waitTime) {
            const startTime = new Date().getTime();
            while ((new Date().getTime() - startTime) < parseInt(waitTime) * 1000) {
            }
            
        }
    );





    Then(
        /^I check the video controls/,
        function(){

            var elements = [
                            'play-pause-button',
                            'rewind-button',
                            'mute-button',
                            'subtitle-button',
                            ]

            for (var i = 0 ; i < elements.length; i++) {

                let elem = UImap.pathFinder(elements[i]);;

                
                if (elem.value == null) {
                    frame.check()
                    pageReload.check()
                    let elem = UImap.pathFinder(elements[i]);
                }
                elem.click();
                browser.wait(0.5);
                elem.click();
                console.log(elements[i],' is working')
            };
            
        }
    );



    Then(
        /^I check that the continue-button is '(enabled|disabled)'$/,
        function(buttonStatus) {
            browser.wait(2)

            let elem = UImap.pathFinder('continue-button');
                
            if (elem.value == null) {
                frame.check()
                pageReload.check()
                let elem = UImap.pathFinder('continue-button');
            }            

            if(buttonStatus == 'enabled') {
                if(elem.getAttribute('disabled')==null){
                    //console.log('element is enabled')                   
                    
                }
                else{
                    throw "element is not enabled"
                }
            }

            if(buttonStatus == 'disabled'){
                if(elem.getAttribute('disabled')){
                    //console.log('element is disabled');
                    
                }
                else{
                    throw "element is enabled"
                }                
            }

        }
    );



/*
        This is for flow test of the side menu, i.e. that the correct courses are available at correct time. 
        e.g Then I check that the 'baseline-adult-child-compressions' is 'enabled'
        NOTE: this does not yet actually test if supposedly disabled menus are in fact disabled
        Will do ASAP
*/
    Then(
        /^I check that the '([^']+)' is '(enabled|disabled|completed)'$/,
        function(scoName,scoStatus){

                let elem = UImap.pathFinder('side-menu-button');
                
                if (elem.value == null) {
                    frame.check()
                    pageReload.check()
                    let elem = UImap.pathFinder('side-menu-button');
                }
                
                elem.click();
                browser.wait(0.5);
              
                if (scoStatus == 'enabled') {
                    elem = UImap.pathFinder(scoName);
                    elem.click();
                    console.log("elem clicked")
                }

                else if (scoStatus == 'disabled'){
                    try {
                        elem = UImap.pathFinder(scoName);
                        elem = UImap.pathFinder('close-side-menu-button');
                        elem.click()
                    }
                    catch (e) {
                        console.log("moving on")
                        if (e instanceof TypeError){
                            console.log("element is diasbled")
                        }
                        else {
                            console.log("something else is wrong")
                            throw "something else is wrong";
                        }
                    }       
                }
                        
        }
    );



/*
    For testing the overview page between baseline modules

        sco-completed
        sco-active
        disabled
        NONE = enabled
*/
    Then(
        /^I check that the baseline overview module '([^']+)' is '(completed|active|inactive)'$/,
        function(scoName, scoStatus) {
            
            let elem = UImap.pathFinder(scoName);

            if (elem.value == null) {
                frame.check()
                pageReload.check()
                let elem = UImap.pathFinder(scoName);
            }
            console.log(elem)

            if(scoStatus == 'completed') {
                if(elem.getAttribute('sco-completed')==""){
                    
                }
                else{
                    throw "element is not completed"
                }
            }

            else if(scoStatus == 'active'){
                if(elem.getAttribute('sco-active')==""){
                    
                }
                else{
                    throw "element is not active"
                }              
            }

            else if(scoStatus == 'inactive'){
                if(elem.getAttribute('sco-completed')==null && elem.getAttribute('sco-active')==null){
                    
                }
                else{
                    throw "element is not inactive"
                }
            }

        }
    );



    Then(
        /^I wait for the video to finish$/, 
        function () {

            let elem = UImap.pathFinder('progress-bar');

            if (elem.value == null) {
                frame.check()
                pageReload.check()
                let elem = UImap.pathFinder('progress-bar');
            }            

            if (elem.value == null) {
                frame.check()
                pageReload.check()
                let elem = UImap.pathFinder('progress-bar');
            }

            while(elem.getAttribute('value') < 100) {
            }
            
        }
    );



    Then(
        /^I activate the virtual manikin$/, 
        function () {
           var waitTime = 3;
           browser.execute("useVirtualManikin()");
           const startTime = new Date().getTime();
           while ((new Date().getTime() - startTime) < waitTime * 1000) {
           }
           
        }
    );



    Then(
        /^I set the virtual manikins '([^']+)' settings to '([^']+)'$/, 
        function (settingsType, settingsValue) {

           let elem = UImap.pathFinder(settingsType);
            if (elem.value == null) {
                frame.check()
                pageReload.check()
                let elem = UImap.pathFinder(settingsType);
            }            

           if(settingsValue=='enabled' || settingsValue=='disabled'){               
               elem.click();
           }
           else{
               var newVal = parseInt(settingsValue)
               elem.setValue(newVal);
           }
           
        }
    );



    Then(
        /^I '(start|stop)' the virtual manikin$/, 
        function (startStop) {

           let elem = UImap.pathFinder('VM-start-stop');

           if (elem.value == null) {
               frame.check()
               pageReload.check()
               let elem = UImap.pathFinder('VM-start-stop');
           }      

           elem.click()
           
        }
    );



    Then(
        /^I wait for the '(baseline|warmup|assessment)' activity to finish/,
        function(activity ) {

            let path = ''

            switch (activity) {
                case 'baseline':
                    /*
                        As you no longer is sent to BL overview after the last 
                        activity, but directly to debriefing, we have to look for a different element
                        This is just a stupid fix, for now.
                    */
                    if (UImap.pathFinder('BL-overview-top-heading').getText() == 'Baseline / Infant Ventilation') {
                        path = 'BL-debrief-heading'
                    }
                    else {
                        path = 'BL-overview-sub-heading'                        
                    }
                    break;

                case 'warmup':
                    path = 'simple-debrief-heading'
                    break;

                case 'assessment':
                    path = 'simple-debrief-heading'
                    break;     

                default:
                    break;
            }

            let elem = UImap.pathFinder(path);

            if (elem.value === null) {
                frame.check()
                let elem = UImap.pathFinder(path);
            }

            while(elem.value === null){ 
                elem = UImap.pathFinder(path);
            }

            
            }
        );


    /*
        Test if score is equal to, above or below X
    */
    Then(
        /^I check that the '([^']+)' score is '(above|below|equal to)' '(\d+)'$/,
        function(selector, operator, score){
            
            let elem = UImap.pathFinder(selector);
            if (elem.value == null) {
                frame.check()
                pageReload.check()
                let elem = UImap.pathFinder(selector);
            } 

            let text = elem.getText()
            let num = text.match(/\d+/)[0]

            console.log("text: " , text)
            console.log('Reg: ' , num)
            console.log(score)

            switch (operator) {
                case "above":
                    if (num > score){
                        console.log("The score is ABOVE the specified")
                        
                    }
                    break;
                
                case "below":
                    if (num < score){
                        console.log("The score is BELOW the specified")
                        
                    }
                    break;

                case "equal to":
                    if(num === score){
                        console.log(num === score)
                        
                    }
                    break;

                default:
                    // code...
                    break;
            }
            }
        );




    Then(
        /^I click the '([^']+)'$/,
        function (selector) {          

          if (selector.includes(">")) {
              console.log("Do old stuff since path is oldschool")
              const selectorArray = selector.split(">");
              const elem = browser.shadowDomElement(["skills-app", "rqi-conductor", ... selectorArray]);
              elem.click()
          }
          else {
              let elem = UImap.pathFinder(selector);
              if (elem.value == null) {
                  frame.check()
                  pageReload.check()
                  let elem = UImap.pathFinder(selector);
              } 
              elem.click()
              browser.wait(1);
          } 
          
        }
	);



	//Used for checking if the text is correct
    //This would be nice if you could check in more languages. 
    Then(
        /^I see if the text in '([^']+)' is similar to '([^']+)'$/, 
        function (selector, text) {

            let elem = UImap.pathFinder(selector);

            if (elem.value === null) {
                frame.check()
                let elem = UImap.pathFinder(selector);
            }

        	let actualText = elem.getText();
        	if (text == actualText) {
	            
	        }
	        else {
	            throw new Error(text + " is not the same as " + actualText);
	        }
        
        }
    );


    //This function does not work yet
    //It is for inserting a number or text into a field
    Then(
        /^I insert '([^']+)' into '([^']+)'$/, 
        function (text,selector) {
        	if (selector.includes(">")) {
	        	const selectorArray = selector.split(">");
	            const elem = browser.shadowDomElement(["va-shell-app", "va-scenario-runner", ... selectorArray]);
	            elem.setValue(text);
	            
        	}
        	else {
        		let elem = UImap.pathFinder(selector);
        		elem.setValue(text)
        		
        	}


        }
    );

    Then(
        /^I debug$/, 
        function (course) {
           browser.debug();
        }
    );


    //This function is used for checking if an element is present 
    Then(
        /^the '([^']+)' is visible/, 
        function (selector) {
          if (selector.includes(">")) {
                const selectorArray = selector.split(">");
                const element = browser.shadowDomElement(["skills-app", "rqi-conductor", ... selectorArray]);
                let isVis = browser.isVisiblePolymer(element); 
                if (isVis) {
                    
                }
                else {
                    throw new Error(`${selector} is not visible`)
                }
          }
          else {
                let elem = UImap.pathFinder(selector);
                let isVis = browser.isVisiblePolymer(elem); //This is not a function according to console output
                if (isVis) {
                    
                }
                else {
                    throw new Error(`${selector} is not visible`)
                }
          } 
        }
    );


    Then(
        /^I see that the course has finished/, 
        function () 
        {
            let elem = browser.element('#close-message > p')

            //if (elem.value == null) {
                //frame.check()
                //pageReload.check()
                //let elem = browser.element('#close-message > p')
            //} 

            let text = elem.getText()

            expect(text).to.include('You are now  and you can exit.');
            
        }
    );  



    Then(
        /^I try that chai thingy/,
        function(){
            let elem = browser.element('body > div > h2');
            expect(elem.getText()).to.include('Welcome');
        }

    )  

