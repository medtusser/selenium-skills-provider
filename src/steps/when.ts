const chai = require("chai")

var { Given, Then, When } = require('cucumber');

import UImap from "../Support/UImap"

/// <reference path="../../custom.d.ts" />


	//TEST for google
    When(
        /^I search for '([^'']+)'$/, 
        function (searchValue) {
        	browser.element("//input[@id='lst-ib']")
        	.setValue(searchValue)
        	browser.click("//button[@name='btnG']")
        }
    );
    When(
        /^I scroll to \(([^,]+),([^\)]+)\)$/,
        function (x, y) {
            browser.scroll(parseInt(x), parseInt(y));
            
        }
    );

