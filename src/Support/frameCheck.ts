/// <reference path="../../custom.d.ts" />

class frame {

    public check() : void {
        //console.log("beginning frameCheck")
       
        const url = browser.getUrl()


        if (url.includes('moodle')){
            //for Moodle, which has ther LTI frame
            if (browser.isVisible('#basicltiLaunchFrame')) {
                browser.frameParent();
                browser.waitForExist('#basicltiLaunchFrame');
                browser.wait(1)
                browser.frame('basicltiLaunchFrame')
                browser.waitForExist('#frame');
                browser.wait(1)
                browser.frame('frame')
                //console.log("Frame has been changed")
            }
            else {
                //console.log('Frame NOT changed')
            }
            console.log('Frame Check..')

            }

        else if (url.includes('contentservice')){
            // for https://cs-prodtest.contentservice.net and https://cs3n-staging.contentservice.net 
            
            browser.frameParent(); 
            
            if (browser.isVisible('#frame')) {
                //console.log(browser.getHTML('#frame',false))
                browser.waitForExist('#frame');
                browser.wait(1)
                browser.frame('frame')
                console.log("Frame has been changed")

            }
            else {
                console.log('Frame NOT changed')
            }

        } 
    }
}

export default new frame()