const chai = require("chai");
const expect = require("chai").expect
var { Given, Then, When } = require('cucumber');

import UImap from "../Support/UImap"
import frame from "../Support/frameCheck"
import pageReload from "../Support/pageReload"

/// <reference path="../../custom.d.ts" />

/**
 * This file is for progress related steps.
 * This includes steps such as e.g. "Given I have completed the Baseline activities"
 * or "Given I launch the course" etc..
 * In other words, any step definitions that relates to the flow from 
 * launch to finishing
 */

Given(
    /^I have started the '([^']+)' course '(with|without)' progress/,
    function (courseName,progress) {
        //start contentservice
        console.log(courseName)
        console.log(1)
        const webPage = "https://service.contentservice.net/#/contentmap";
        console.log(2)        
        browser.url(webPage);
        console.log(3)
        browser.wait(3)  
        //login for Laerdal..     
        console.log(4)
        browser.setValue('#i0116','glen.videmark@laerdal.com')
        console.log(5)
        browser.click('#idSIButton9')
        console.log(6)
        browser.wait(5)
        console.log(7)
        // enter the course with/without progrss
        browser.wait(2)
        console.log(8)
        browser.click('body > nav > div > ul > li:nth-child(5) > a')
        console.log(9)
        var elem = browser.element('body > div > div:nth-child(4) > div > input')
        console.log(elem)
        elem.setValue(courseName)
        console.log(courseName)
        browser.wait(2)
        //click specific course (skills_2015_rqi_web_prep_q4)
        console.log(10)
        browser.click('body > div > table > tbody > tr > td:nth-child(1) > a:nth-child(6)');
        console.log(11)
        browser.wait(2)
        console.log(12)
        browser.click('#launchDialog > div > div > div.modal-body > form > div:nth-child(4) > div > button:nth-child(2)')
        console.log(13)
        //click user-defined
        browser.wait(2)
        console.log(14)
        browser.click('#wrapper > div:nth-child(1) > div > button.btn.btn-sm.btn-default')
        console.log(15)
        browser.wait(3)
        //set randrom name
        console.log(16)
        var randomText = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 10; i++){
          randomText += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        console.log("Username: ",randomText)
        browser.setValue('#wrapper > div:nth-child(5) > input',randomText)            
        browser.click('#launchDialog > div > div > div.modal-footer > button.btn.btn-danger')
        browser.wait(5)
        browser.switchTab(browser.getTabIds()[1])        

    }
);