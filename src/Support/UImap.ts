/// <reference path="../../custom.d.ts" />

class UImap {

    public pathFinder(leEelement){
    	//const slide = browser.getUrl().slice(-2);
        var path = {};

        // -------------------------PROVIDER-------------------------
        path['continue-button']                   = "skills-bottombar>#provider-continue-button"

        // VIDEO CONTROLS
        path["play-button"]                       = "skills-view-video>oxygen-videoplayer>[data-plyr='play']"
        path["play-pause-button"]                 = "skills-bottombar>#playPauseButton"   
        path["rewind-button"]                     = "skills-bottombar>#replayButton"
        path["mute-button"]                       = "skills-bottombar>#muteButton"
        path["subtitle-button"]                   = "skills-bottombar>#subtitlesButton"
        path["progress-bar"]                      = "skills-bottombar>skills-slider"

        // SIDE MENU CONTROLS
        path["side-menu-button"]                  = "rqi-topbar>.menu-button"
        path["close-side-menu-button"]            = "oxygen-sco-menu>.arrowIcon"
        path["BL-introduction"]                   = "oxygen-sco-menu>#skills-web-introduction-baseline"
        path["BL-adult-child-comp"]               = "oxygen-sco-menu>#skills-web-adult-compressions-baseline"
        path["BL-adult-child-vent"]               = "oxygen-sco-menu>#skills-web-adult-ventilations-baseline"
        path["BL-infant-comp"]                    = "oxygen-sco-menu>#skills-web-infant-compressions-baseline"           
        path["BL-infant-vent"]                    = "oxygen-sco-menu>#skills-web-infant-ventilations-baseline"              
        path["BL-debriefing"]                     = "oxygen-sco-menu>#skills-web-results-baseline" 

        path["CORE-adult-child-comp"]             = "oxygen-sco-menu>#skills-web-adult-compressions"
        path["CORE-adult-child-vent"]             = "oxygen-sco-menu>#skills-web-adult-ventilations"
        path["CORE-infant-comp"]                  = "oxygen-sco-menu>#skills-web-infant-compressions"           
        path["CORE-infant-vent"]                  = "oxygen-sco-menu>#skills-web-infant-ventilations"                 

        path['previous-results']                  = ""

        // VIRTUAL MANIKIN CONTROLS
        path['VM-comp']                           = "oxygen-virtual-manikin>[name='compressions']"
        path['VM-vent']                           = "oxygen-virtual-manikin>[name='ventilations']"
        path['VM-CPR']                            = "oxygen-virtual-manikin>[name='cpr']"
        path['VM-type-adult']                     = "oxygen-virtual-manikin>[name='adult']"
        path['VM-type-infant']                    = "oxygen-virtual-manikin>[name='infant']"
        path['VM-comp-depth']                     = "oxygen-virtual-manikin>[label='Depth (mm)']>[aria-labelledby='paper-input-label-1']"
        path['VM-comp-release']                   = "oxygen-virtual-manikin>[label='Release (mm)']>[aria-labelledby='paper-input-label-2']"
        path['VM-comp-rate']                      = "oxygen-virtual-manikin>[label='Rate (cpm)']>[aria-labelledby='paper-input-label-3']"
        // [aria-labelledby='paper-input-label-4'] is for stopping compressions
        path['VM-vent-volume']                    = "oxygen-virtual-manikin>[label='Volume (ml)']>[aria-labelledby='paper-input-label-5']"
        path['VM-vent-rate']                      = "oxygen-virtual-manikin>[label='Rate (vpm)']>[aria-labelledby='paper-input-label-6']"
        // [aria-labelledby='paper-input-label-7'] is for stopping ventilations
        path['VM-comp-handplacement']             = "oxygen-virtual-manikin>paper-toggle-button>#toggleButton"
        path['VM-start-stop']                     = "oxygen-virtual-manikin>#startStopEvents"

        // (CORE SKILLS) OVERVIEW CONTROLS
        path['overview-heading']                  = "rqi-view-overview>#viewOverviewHeader"
        path['overview-adult-child-comp']         = "rqi-view-overview>li:nth-child(1)"
        path['overview-adult-child-vent']         = "rqi-view-overview>li:nth-child(2)"
        path['overview-infant-comp']              = "rqi-view-overview>li:nth-child(3)"
        path['overview-infant-vent']              = "rqi-view-overview>li:nth-child(4)"

        // (BASELINE) OVERVIEW CONTROLS
        path['BL-overview-top-heading']              = "rqi-topbar>h2"
        path['BL-overview-sub-heading']              = "rqi-view-overview>h1"      
        
        // ACTIVITY CONTROLS
        path['BL-dots']                              = "rqi-view-baseline-live>skills-dots-activity-indicator"

        // BASELINE DEBRIEFING OVERVIEW CONTROLS
        path['BL-debrief-heading']                   = "skills-view-baseline-debriefing>h1"

        path['BL-adult-comp-debrief']                = "skills-view-baseline-debriefing>paper-button:nth-child(1)"
        path['BL-adult-vent-debrief']                = "skills-view-baseline-debriefing>paper-button:nth-child(2)"
        path['BL-infant-comp-debrief']               = "skills-view-baseline-debriefing>paper-button:nth-child(3)"
        path['BL-infant-vent-debrief']               = "skills-view-baseline-debriefing>paper-button:nth-child(4)"


        path['BL-advanced-debrief']                  = "rqi-popup-simple-debrief>skills-simple-debrief>#detailsBtn"

        // BASELINE DEBRIEFING CONTROLS
        path['BL-simple-debrief-result']             = "rqi-popup-simple-debrief>skills-simple-debrief>h2"
        path['BL-advanced-debrief-result']           = "rqi-popup-advanced-debriefing>h2"
        path['BL-simple-debrief-close-button']       = "rqi-popup-simple-debrief>paper-icon-button"  
        path['BL-advanced-debrief-close-button']     = "rqi-popup-advanced-debriefing>paper-icon-button"   

        // GENERAL ELEMENTS AND CONTROLS
        path['instruction-page-title']               = "skills-view-intro>h1"
        path['instruction-page-activity-title']      = "skills-view-intro>h2"
        
        // DEBRIEFINGS (WARMUP and ASSESSMENT)
        path['simple-debrief-heading']               = "skills-view-debriefing>h1"
        path['advanced-debrief']                     = "skills-view-debriefing>#details-button"  
        path['simple-debrief-result']                = "skills-view-debriefing>skills-view-debriefing-visual-score>#user-score-text"
        path['advanced-debrief-result']              = "rqi-popup-advanced-debriefing>h2"
        path['advanced-debrief-close-button']        = "rqi-popup-advanced-debriefing>paper-icon-button"   


        const leElement = path[leEelement]

        const selectorArray = leElement.split(">");
        let element = browser.shadowDomElement(["skills-conductor", ... selectorArray]);
        return element;
    }
}
export default new UImap()