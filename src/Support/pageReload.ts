/// <reference path="../../custom.d.ts" />
import UImap from "../Support/UImap"

//https://stackoverflow.com/questions/12967541/how-to-avoid-staleelementreferenceexception-in-selenium 

class pageReload {

    public check() : void {
        var result = false;
        var attempts = 0;
        //let elem = UImap.pathFinder('side-menu-button');
        while(attempts < 10) {
            try {
                browser.isVisible('#frame');
                result = true;
                break;
            } catch(StaleElementException) {
                console.log("wut")
            }
            attempts++;
        }
    }
}
export default new pageReload()