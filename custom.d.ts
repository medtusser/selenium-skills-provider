declare namespace WebdriverIO {
    export interface Client<T> {
		isVisiblePolymer(
	        element: string,
	        milliseconds?: number
	    ): Client<boolean> & boolean;

	    isMuted(
	        element: string,
	        milliseconds?: number,
	    ): Client<boolean> & boolean;

	    wait(
	        milliseconds?: number,
	    ): Client<boolean> & boolean;
	    
	    shadowDomElement(
            selector: string[],
            milliseconds?: number,
            reverse?: boolean
        ): Client<any> & any;
	}
}