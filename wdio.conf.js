// <reference path="../../custom.d.ts" />
const fs = require("fs")
// Wait this amount of ms between each driver command.
const COMMAND_WAIT_TIME_BEFORE = 400
const COMMAND_WAIT_TIME_AFTER = 400
const manikinExtension = '--load-extension=C:/Users/' + require("os").userInfo().username + 
'/AppData/Local/Google/Chrome/User Data/Default/Extensions/nbibecoggfbkbaleodhbnkibffgabpdo/1.0.6_1';
exports.config = {
    //
    // ==================
    // Specify Test Files
    // ==================
    // Define which test specs should run. The pattern is relative to the
    // directory from which `wdio` was called. Notice that, if you are calling
    // `wdio` from an NPM script (see https://docs.npmjs.com/cli/run-script)
    // then the current working directory is where your package.json resides, so
    // `wdio` will be called from there.
    //
    specs: [
        //'./features/**/*.feature',
        //'./features/show-case.feature',
        //'./features/ManikinConnection/connection-succesful.feature',
        //'./features/ManikinConnection/connection-unsuccesful.feature',
        //'./features/responder-flow-test-full.feature',
        //'./features/test.feature',
        './features/newTest.feature'
    ],
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    //
    // ============
    // Capabilities
    // ============
    // Define your capabilities here. WebdriverIO can run multiple capabilities
    // at the same time. Depending on the number of capabilities, WebdriverIO
    // launches several test sessions. Within your capabilities you can
    // overwrite the spec and exclude options in order to group specific specs
    // to a specific capability.
    //
    // First, you can define how many instances should be started at the same
    // time. Let's say you have 3 different capabilities (Chrome, Firefox, and
    // Safari) and you have set maxInstances to 1; wdio will spawn 3 processes.
    // Therefore, if you have 10 spec files and you set maxInstances to 10, all
    // spec files will get tested at the same time and 30 processes will get
    // spawned. The property handles how many capabilities from the same test
    // should run tests.
    //
    maxInstances: 5,
    //
    // If you have trouble getting all important capabilities together, check
    // out the Sauce Labs platform configurator - a great tool to configure your
    // capabilities: https://docs.saucelabs.com/reference/platforms-configurator
    //
    capabilities: [{
        // maxInstances can get overwritten per capability. So if you have an
        // in-house Selenium grid with only 5 firefox instance available you can
        // make sure that not more than 5 instance gets started at a time.
        maxInstances: 1,
        //
        browserName: 'chrome',
        chromeOptions: {
            args: ["--start-maximized", "--mute-audio", "--disable-infobars", "debug-enable-frame-toggle",'--window-size=1920,1080', manikinExtension],
        }
    }],
    //
    // ===================
    // Test Configurations
    // ===================
    // Define all options that are relevant for the WebdriverIO instance here
    //
    // By default WebdriverIO commands are executed in a synchronous way using
    // the wdio-sync package. If you still want to run your tests in an async
    // way e.g. using promises you can set the sync option to false.
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result |
    // error
    logLevel: 'error',
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Saves a screenshot to a given path if a command fails.
    //screenshotPath: './errorShots/',
    //
    // Set a base URL in order to shorten url command calls. If your url
    // parameter starts with "/", then the base url gets prepended.
    //baseUrl: "https://google.dk",
    //baseUrl: "https://google.dk",
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 90000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    //
    // Initialize the browser instance with a WebdriverIO plugin. The object
    // should have the plugin name as key and the desired plugin options as
    // properties. Make sure you have the plugin installed before running any
    // tests. The following plugins are currently available:
    // WebdriverCSS: https://github.com/webdriverio/webdrivercss
    // WebdriverRTC: https://github.com/webdriverio/webdriverrtc
    // Browserevent: https://github.com/webdriverio/browserevent
    // plugins: {
    //     webdrivercss: {
    //         screenshotRoot: 'my-shots',
    //         failedComparisonsRoot: 'diffs',
    //         misMatchTolerance: 0.05,
    //         screenWidth: [320,480,640,1024]
    //     },
    //     webdriverrtc: {},
    //     browserevent: {}
    // },
    //
    // Test runner services
    // Services take over a specific job you don't want to take care of. They
    // enhance your test setup with almost no effort. Unlike plugins, they don't
    // add new commands. Instead, they hook themselves up into the test process.
    services: ['selenium-standalone'],
    //
    // Framework you want to run your specs with.
    // The following are supported: Mocha, Jasmine, and Cucumber
    // see also: http://webdriver.io/guide/testrunner/frameworks.html
    //
    // Make sure you have the wdio adapter package for the specific framework
    // installed before running any tests.
    framework: 'cucumber',
    //
    // Test reporter for stdout.
    // The only one supported by default is 'dot'
    // see also: http://webdriver.io/guide/testrunner/reporters.html
    reporters: ['cucumber'],
    //reporters: ['spec'],
    //
    // If you are using Cucumber you need to specify the location of your step
    // definitions.

    cucumberOpts: {
        require: [
            './build/steps/*.js',
        ], // <string[]> (file/dir) require files before executing features
        backtrace: false, // <boolean> show full backtrace for errors
        compiler: [
            'js:babel-register',
        ], // <string[]> ("extension:module") require files with the given
           // EXTENSION after requiring MODULE (repeatable)
        dryRun: false, // <boolean> invoke formatters without executing steps
        failFast: false, // <boolean> abort the run on first failure
        format: ['pretty'], // <string[]> (type[:path]) specify the output
                            // format, optionally supply PATH to redirect
                            // formatter output (repeatable)
        colors: true, // <boolean> disable colors in formatter output
        snippets: true, // <boolean> hide step definition snippets for pending
                        // steps
        source: true, // <boolean> hide source uris
        profile: [], // <string[]> (name) specify the profile to use
        strict: true, // <boolean> fail if there are any undefined or pending
                       // steps
        //tags: require('./src/tagProcessor')(process.argv), ORIGINAL FROM BOILERPLATE
        //tags: [],
        // <string[]> (expression) only execute the features or scenarios with
        // tags matching the expression
        timeout: 20000000,     // <number> timeout for step definitions
        ignoreUndefinedDefinitions: false, // <boolean> Enable this config to
                                           // treat undefined definitions as
                                           // warnings.
    },

    mochaOpts: {
        ui: 'tdd',
        // compilers: ['ts:ts-node/register'],
        timeout: 999999999
    },

    //
    // =====
    // Hooks
    // =====
    // WebdriverIO provides several hooks you can use to interfere with the test
    // process in order to enhance it and to build services around it. You can
    // either apply a single function or an array of methods to it. If one of
    // them returns with a promise, WebdriverIO will wait until that promise got
    // resolved to continue.
    //
    // Gets executed once before all workers get launched.
    // onPrepare: function onPrepare(config, capabilities) {
    // },
    //
    // Gets executed before test execution begins. At this point you can access
    // all global variables, such as `browser`. It is the perfect place to
    // define custom commands.
    before: function before() {
        /**
         * Setup the Chai assertion framework
         */
        // require('ts-node/register');
       const chai = require('chai');

        global.expect = chai.expect;
        global.assert = chai.assert;
        global.should = chai.should();


        browser.addCommand("wait", function (waitTime){
            const startTime = new Date().getTime();
            while ((new Date().getTime() - startTime) < parseInt(waitTime) * 1000) {
            } 
        })

        browser.addCommand("isVisiblePolymer", function(element) {
            const styleAttr = element.getAttribute("style");
            const ariaAttr = element.getAttribute("aria-disabled");
            const visibility = styleAttr.indexOf("visibility: visible;") > -1;
            const display = styleAttr.indexOf("display: flex;") > -1;
            const aria = ariaAttr;
            return (display || visibility || aria);
        })


        /**
         * Add a command to return an element within a shadow dom.
         * The command takes an array of selectors. Each subsequent
         * array member is within the preceding element's shadow dom.
         *
         * Example:
         *
         *     const elem = browser.shadowDomElement(['foo-bar', 'bar-baz', 'baz-foo']);
         *
         * Browsers which do not have native ShadowDOM support assume each selector is a direct
         * descendant of the parent.
         */
        browser.addCommand("shadowDomElement", function(selector) {
            return this.execute(function(selectors) {
                if (!Array.isArray(selectors)) {
                  selectors = [selectors];
                }

                function findElement(selectors) {
                    var currentElement = document;
                    for (var i = 0; i < selectors.length; i++) {
                        if (i > 0) {
                            currentElement = currentElement.shadowRoot;
                        }
                        currentElement = currentElement.querySelector(selectors[i]);
                        if (!currentElement) {
                            break;
                        }
                    }
                    return currentElement;
                }

                if (!(document.body.createShadowRoot || document.body.attachShadow)) {
                  selectors = [selectors.join(' ')];
                }
                return findElement(selectors);

          }, selector);
        });
    },
    beforeScenario: function beforeScenario(scenario) {
        browser.windowHandleMaximize()
    },    
    //
    // Hook that gets executed before the suite starts
    // beforeSuite: function beforeSuite(suite) {
    // },
    //
    // Hook that gets executed _before_ a hook within the suite starts (e.g.
    // runs before calling beforeEach in Mocha)
    // beforeHook: function beforeHook() {
    // },
    //
    // Hook that gets executed _after_ a hook within the suite starts (e.g. runs
    // after calling afterEach in Mocha)
    // afterHook: function afterHook() {
    // },
    //
    // Function to be executed before a test (in Mocha/Jasmine) or a step (in
    // Cucumber) starts.
    beforeTest: function beforeTest(test) {
        console.log("beforeTest")
        console.log(test)
    },
    //
    // Runs before a WebdriverIO command gets executed.
    beforeCommand: function beforeCommand(commandName, args) {
        const p = new Promise((res,rej) => {
            setTimeout(res, COMMAND_WAIT_TIME_BEFORE);
        })
        return p;
    },
    //
    // Runs after a WebdriverIO command gets executed
    afterCommand: function afterCommand(commandName, args, result, error) {
        const p = new Promise((res,rej) => {
            setTimeout(res, COMMAND_WAIT_TIME_AFTER);
        })
        return p;
    },
    //
    // Function to be executed after a test (in Mocha/Jasmine) or a step (in
    // Cucumber) starts.
    afterTest: function afterTest(test) {
        console.log("HALLO? ")
    },


    onError: function(e) {
        console.log(e);
         throw new Error("my error message");
    },
    //afterStep: function afterStep(step) {
        //const status = step.getStatus()
        //if (status === "passed" || status === "skipped") {
            //return;
        //}
        //else {
            //console.log("NOT passed");
            //// Make somewhat readable filename, that also shows where error occured.
            //var filename = encodeURIComponent(
                //step.getStep().getName()
                //.replace(/[^A-Za-z0-9]+/g, '_')
            //);
            //console.log("filename: " + filename);
            //const today = new Date()
            //// build file path
            //var filePath = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}_${filename}`;
            //console.log(filePath);
            //// save screenshot
            //const imgData = browser.saveScreenshot(`./errorShots/${filePath}.png`);
            //const html =
                //`<html><head></head>
                    //<body>
                        //<h3>
                            //${step.getFailureException().message}
                        //</h3>
                        //<h3>
                            //URL: ${browser.getUrl()}
                        //</h3>
                        //<h3>
                            //Step: ${step.getStep().getName()}
                        //</h3>
                        //<img src='${filePath}.png'/>
                    //</body>
                //</html>`
//
            //fs.writeFileSync(`./errorShots/${filePath}.html`, html)
            //console.log('\n\tScreenshot location:', `./errorShots/${filePath}.html`, '\n');
        //}
//
    //},
    //
    // Hook that gets executed after the suite has ended
    // afterSuite: function afterSuite(suite) {
    // },
    //
    // Gets executed after all tests are done. You still have access to all
    // global variables from the test.
    // after: function after(result, capabilities, specs) {
    // },
    //
    // Gets executed after all workers got shut down and the process is about to
    // exit. It is not possible to defer the end of the process using a promise.
    // onComplete: function onComplete(exitCode) {
    // }
};
